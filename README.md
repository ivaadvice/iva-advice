IVA Advice provide free debt advice to people in the UK.

An Individual Voluntary Arrangement (IVA) is a formal debt solution which you can consolidate all of your debts and pay one low affordable monthly payment to an Insolvency Practitioner (IP).